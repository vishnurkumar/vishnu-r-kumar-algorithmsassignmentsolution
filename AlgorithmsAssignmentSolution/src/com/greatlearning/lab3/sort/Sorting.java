package com.greatlearning.lab3.sort;

public class Sorting {
		
		void mergeAsc(double stockPrice[], int start, int middle, int end)
	    {
	        int n1 = middle - start + 1;
	        int n2 = end - middle;
	 
	        double L[] = new double [n1];
	        double R[] = new double [n2];
	 
	        for (int i=0; i<n1; ++i)
	            L[i] = stockPrice[start + i];
	        for (int j=0; j<n2; ++j)
	            R[j] = stockPrice[middle + 1+ j];
	 
	        int i = 0, j = 0;int k = start;
	        
	        while (i < n1 && j < n2)
	        {
	            if (L[i] <= R[j])
	            {
	            	stockPrice[k] = L[i];
	                i++;
	            }
	            else
	            {
	            	stockPrice[k] = R[j];
	                j++;
	            }
	            k++;
	        }
	 
	        while (i < n1)
	        {
	        	stockPrice[k] = L[i];
	            i++;
	            k++;
	        }
	 
	        while (j < n2)
	        {
	        	stockPrice[k] = R[j];
	            j++;
	            k++;
	        }
	    }
		
		public void mergeSortAsc(double[] stockPrice, int start, int end) {
			if (start < end)
	        {
	            int middle = (start + end)/2;
	            mergeSortAsc(stockPrice, start, middle);
	            mergeSortAsc(stockPrice , middle + 1, end);

	            mergeAsc(stockPrice, start, middle, end);
	        }
		}
		
		public double[] sortAscending(double[] stockPrice, int length) {
			int start = 0;
			int end = length - 1;
			mergeSortAsc(stockPrice, start, end);
			
			return stockPrice;
		}

	}

