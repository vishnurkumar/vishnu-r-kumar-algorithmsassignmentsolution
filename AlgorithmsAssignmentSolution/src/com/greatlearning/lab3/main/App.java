package com.greatlearning.lab3.main;

import java.util.Scanner;
import com.greatlearning.lab3.sort.Sorting;
import com.greatlearning.search.Search;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int count = 0;
		
		System.out.println("Enter the no of Companies");
		int noOfComp = sc.nextInt();
		double stockPrice[] = new double[noOfComp];
		char stockCompare[] = new char[noOfComp];
		
		for (int i = 0 ; i < noOfComp ; i ++ ) {
			System.out.println("Enter the stock price of the company " + (i+1) + ": ");
			stockPrice[i] = sc.nextDouble();
			System.out.println("Whether company's stock price rose today compare to yesterday? (y / n)");
			int flag = 0;
			while(flag == 0) {
				char userValue = sc.next().charAt(0);
				switch(userValue) {
				case 'y' :
					stockCompare[i] = userValue;
					count += 1;
					flag = 1;
					break;
				case 'n' :
					stockCompare[i] = userValue;
					flag = 1;
					break;
				default :
					break;
				}
			}
		}
		
		System.out.println("----------------------------------------------------------------------------");
		System.out.println("Enter the operation that you want to perform");
		System.out.println("1. Display the companies stock prices in ascending order");
		System.out.println("2. Display the companies stock prices in descending order");
		System.out.println("3. Display the total no of companies for which stock prices rose today");
		System.out.println("4. Display the total no of companies for which stock prices declined today");
		System.out.println("5. Search a specific stock price");
		System.out.println("6. press 0 to exit");
		int operation = sc.nextInt();
		Sorting obj = new Sorting();
		int flag = 0;
		while (flag == 0) {
		switch (operation) {
			case 1 :
				double[] result = obj.sortAscending(stockPrice, noOfComp);
				if(result != null) {
					System.out.println("Stock prices in ascending order are");
					for(int i=0; i < noOfComp; i++ ) {
						System.out.println(result[i]);
					}
				}
			break;
			case 2 :
				double[] results = obj.sortAscending(stockPrice, noOfComp);
				if(results != null) {
					System.out.println("Stock prices in descending order are");
					for(int i = noOfComp-1 ; i >= 0; i--) {
						System.out.println(results[i]);
					}
				}
			break;
			case 3 :
				System.out.println("Total no of companies whose stock price rose today : " + count);
			break;
			case 4 :
				System.out.println("Total no of companies whose stock price declined today : " + (noOfComp - count));
			break;
			case 5:
				System.out.println("enter the key value");
				int searchValue = sc.nextInt();
				Search newObj = new Search();
				double search = newObj.search(stockPrice, searchValue);

		        if (search == -1)
		            System.out.println("Element not present");
		        else
		            System.out.println("Element found at index "
		                               + search);
			break;
			case 0:
			flag =1; 
			break;				
				
			}
		}
		sc.close();

	}

}
