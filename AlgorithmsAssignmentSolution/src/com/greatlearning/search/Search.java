package com.greatlearning.search;

public class Search {
	
	public double search(double[] stockPrice , double value){
	
	        int l = 0, r = stockPrice.length - 1;
	 
	        while (l <= r) {
	            int m = l + (r - l) / 2;
	 
	            // Check if x is at mid
	            if (stockPrice[m] == value)
	                return m;
	 
	            if (stockPrice[m] < value)
	                l = m + 1;
	 
	            else
	                r = m - 1;
	        }
	        return -1;
	    }
}
